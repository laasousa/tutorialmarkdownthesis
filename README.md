# R Markdown Thesis Tutorial

*R Markdown* is a great way to include data analysis and modelling results directly within a thesis or report.
I wrote this tutorial since it can be a little tricky to produce a thesis document which contains properly formatted text, equations, tables, figures, images, page numbers, appendix, and so on.

The *skeletonThesis.Rmd* file in this repository can be directly used as a skeleton for a thesis; also see its corresponding *skeletonThesis.pdf*.
Even if you are familiar with *R Markdown* and *LaTex* I would recommend having a quick read through my tutorial [https://achale.gitlab.io/tutorialmarkdownthesis/](https://achale.gitlab.io/tutorialmarkdownthesis/).


[Dr Alison Hale](https://www.lancaster.ac.uk/staff/haleac/) wrote this tutorial while undertaking a Medical Research Council *Skills Development Fellowship* in biostatistics and health informatics at Lancaster University, UK.
As part of this fellowship Dr Hale undertook a MSc (by Research), this qualification was gained by writing a thesis of around 30,000 words; no exams other than a viva.
